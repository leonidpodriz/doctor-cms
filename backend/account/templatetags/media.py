from django import template
from doctor_cms.settings import MEDIA_URL

register = template.Library()


@register.simple_tag(name="media")
def media(file_path):
    return MEDIA_URL + str(file_path)
