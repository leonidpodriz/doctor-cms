from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    city = models.TextField(blank=True)
    phone = models.TextField(blank=True)
    avatar = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True)
