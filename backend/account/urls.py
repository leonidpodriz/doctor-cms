from django.urls import path

from .views import Login, Register, logout_page, profile_page, profile_page_edit

urlpatterns = [
    path('login/', Login.as_view(), name="login"),
    path('register/', Register.as_view(), name="register"),
    path('logout/', logout_page, name="logout"),
    path('profile/', profile_page, name="profile"),
    path('profile-edit/', profile_page_edit, name="profile-edit"),
]