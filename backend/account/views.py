from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import FormView

from .forms import AuthForm, RegisterForm, ProfileForm, UserEditForm
from .models import Profile


class Login(FormView):
    form_class = AuthForm
    template_name = 'account/form.html'
    extra_context = {
        'form_name': "Вход",
    }
    success_url = reverse_lazy("dashboard")

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user=user)

        return super().form_valid(form)


class Register(FormView):
    form_class = RegisterForm
    template_name = 'account/form.html'
    extra_context = {
        'form_name': "Регистрация",
    }
    success_url = reverse_lazy("dashboard")

    def form_valid(self, form):
        form.save()

        return super().form_valid(form)


def logout_page(request):
    user = request.user

    if user:
        logout(request)

    return redirect("/")


@login_required
def profile_page(request):
    profile = Profile.objects.get(user=request.user)
    profile_form = ProfileForm(instance=profile)
    user_form = UserEditForm(instance=request.user)
    Profile.objects.get_or_create(user=request.user)
    profile = Profile.objects.get(user=request.user)
    context = {
        "fields": {
            "Username": profile.user.username,
            "Email": profile.user.email,
            "Город": profile.city,
            "Телефон": profile.phone,
        },
        "forms": [user_form, profile_form],
        "avatar": profile.avatar,
    }
    return render(request, 'account/profile.html', context)


@login_required
def profile_page_edit(request):
    Profile.objects.get_or_create(user=request.user)
    profile = Profile.objects.get(user=request.user)
    print(request.FILES)
    if request.method == "POST":
        profile_form = ProfileForm(request.POST, request.FILES, instance=profile)
        user_form = UserEditForm(request.POST, request.FILES, instance=request.user)

        if profile_form.is_valid() and user_form.is_valid():
            profile_form.save()
            user_form.save()
            return redirect('profile')

    else:
        profile_form = ProfileForm(instance=profile)
        user_form = UserEditForm(instance=request.user)

    context = {
        "forms": [user_form, profile_form],
    }

    return render(request, 'account/profile_edit.html', context)
