from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import RedirectView, FormView, ListView

from .froms import AddNewVisitForm
from .models import DoctorVisit


class MainPage(LoginRequiredMixin, RedirectView):
    url = reverse_lazy("dashboard")


class DashboardView(LoginRequiredMixin, ListView):
    model = DoctorVisit
    template_name = 'dashboard/index.html'

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class AddNewVisitView(LoginRequiredMixin, FormView):
    form_class = AddNewVisitForm
    template_name = 'dashboard/form.html'
    success_url = reverse_lazy('dashboard')

    def form_valid(self, form):
        visit = form.save(commit=False)
        visit.user = self.request.user
        visit.save()

        return super().form_valid(form)
