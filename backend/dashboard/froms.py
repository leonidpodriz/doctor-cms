from django import forms

from dashboard.models import DoctorVisit


class AddNewVisitForm(forms.ModelForm):
    class Meta:
        model = DoctorVisit
        fields = "__all__"
        exclude = ("user", )
        widgets = {
            'data_time': forms.DateTimeInput(attrs={'type': "datetime-local"})
        }