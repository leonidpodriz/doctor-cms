from django.urls import path

from .views import DashboardView, MainPage, AddNewVisitView

urlpatterns = [
    path("", MainPage.as_view(), name="main"),
    path("dashboard", DashboardView.as_view(), name="dashboard"),
    path("dashboard/new-visit", AddNewVisitView.as_view(), name="new-visit"),
]
