from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from dashboard.models import DoctorVisit


def get_card_status(request, pk):
    visit = get_object_or_404(DoctorVisit, pk=pk)
    STATUSES = {
        'c': "Одобрено",
        'w': "Ожидает",
    }
    return JsonResponse({"status": STATUSES[visit.status]}, charset="utf-8")
