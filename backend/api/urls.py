from django.urls import path

from .views import get_card_status

urlpatterns = [
    path("getCardStatus/<int:pk>", get_card_status, name="api")
]
